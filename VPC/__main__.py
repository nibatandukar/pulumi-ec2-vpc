
import pulumi
from pulumi_aws import ec2, get_availability_zones

import pulumi_aws as aws

import utils


# read local config settings
config = pulumi.Config()

private_subnet_cidrs = config.require_object("private_subnet_cidrs")
public_subnet_cidrs = config.require_object("public_subnet_cidrs")
#private_subnet_cidrs = ['172.255.10.0/24']
zones_amount = config.require_int("zones_amount")

zones = utils.get_aws_az(zones_amount)

vpc = ec2.Vpc(
    "pulumi-vpc",
    cidr_block=config.require("vpc_cidr"),
    tags={"Name": "pulumi-vpc"}
)

igw = ec2.InternetGateway("pulumi-igw",
        vpc_id=vpc.id)

public_rt = ec2.RouteTable(
    "pulumi-public-rt",
    vpc_id=vpc.id,
    routes=[{"cidr_block": "0.0.0.0/0", "gateway_id": igw.id}],
    tags={"Name": "pulumi-public-rt"},
)

public_subnet_ids = ['public_subnet_cidrs']
private_subnet_ids = ['private_subnet_cidrs']

for zone, private_subnet_cidr, public_subnet_cidr in zip(
    zones, private_subnet_cidrs, public_subnet_cidrs
):

#####################   Public stuff    ###########################################

    public_subnet = ec2.Subnet(
        f"pulumi-public-subnet-{zone}",
        assign_ipv6_address_on_creation=False,
        vpc_id=vpc.id,
        map_public_ip_on_launch=True,
        cidr_block=public_subnet_cidr,
        availability_zone=zone,
        tags={"Name": f"pulumi-public-subnet-{zone}"},
    )
    ec2.RouteTableAssociation(
        f"pulumi-public-rta-{zone}",
        route_table_id=public_rt.id,
        subnet_id=public_subnet.id,
    )
    public_subnet_ids.append(public_subnet.id)

######################## Private stuff ##########################################

    private_subnet = ec2.Subnet(
        f"pulumi-private-subnet-{zone}",
        assign_ipv6_address_on_creation=False,
        vpc_id=vpc.id,
        map_public_ip_on_launch=False,
        cidr_block=private_subnet_cidr,
        availability_zone=zone,
        tags={"Name": f"pulumi-private-subnet-{zone}"},
    )
    eip = ec2.Eip(f"pulumi-eip-{zone}", tags={"Name": f"pulumi-eip-{zone}"})
    nat_gateway = ec2.NatGateway(
        f"pulumi-natgw-{zone}",
        subnet_id=public_subnet.id,
        allocation_id=eip.id,
        tags={"Name": f"pulumi-natgw-{zone}"},
    )
    private_rt = ec2.RouteTable(
        f"pulumi-private-rt-{zone}",
        vpc_id=vpc.id,
        routes=[{"cidr_block": "0.0.0.0/0", "gateway_id": nat_gateway.id}],
        tags={"Name": f"pulumi-private-rt-{zone}"},
    )
    ec2.RouteTableAssociation(
        f"pulumi-private-rta-{zone}",
        route_table_id=private_rt.id,
        subnet_id=private_subnet.id,
    )
    private_subnet_ids.append(private_subnet.id)


##########################    INSTANCES    ###############################

deployer = aws.ec2.KeyPair("deployer", public_key="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQD2KjDe2L8jwHBviGj+amp7dujHrFtabIp3hGUWUt3TnJ9RSxQSRmEDL2N9Gohc3m2MXR0Zxpzhu+FXJLUraHFFhbV4JWcBGEOKMSmCBoTKBAE1sJw9hb4VPKWWmIgrnMMdDTnC9OXjRVn/sqLteDAINcMhfAajNQlFdS+uty6f2VCHNiLq9QTNqgbglGQ2FXseJYoMqeaoikFPinAHp7vsomVSYbNNy3vVbQkUUxn8/msAJBLO77j8DnuqnftSphCUdNNyhF3TIk2BR+4xH5QxpUUOnQ8EL6xmSrkzepRSTFE5SVEoGD238s+qlkSxSOhZKfss2B/G4uPXEnKpCfBWMn3egW4NJFaNY/OL2cosfw23mKOIqD8IcWSgnI7SecsAvW8CehsHHjpXOjYkJ0jeTCNlueEsUxLGv5F4mzj57o4C69uhiZaQmMA02dIjjpC8hMBVutF8Df07ThSe1ovTVdwesZhmnIvDTh96lqk/s520DAWBs6IKT6LHozXgsAc= root@niba-Inspiron-3543")


ami = aws.get_ami(
    most_recent="true",
    owners=['amazon'],
    filters=[{'name': 'name', 'values': ['amzn-ami-hvm-*']}]
)

sg = aws.ec2.SecurityGroup(
    "VPC-SG",
    vpc_id=vpc.id,
    description="VPC SG",
    ingress=[aws.ec2.SecurityGroupIngressArgs(
        description="SSH PORT",
        from_port=22,
        to_port=22,
        protocol="tcp",
        cidr_blocks=["0.0.0.0/0"],
    )],
    egress=[aws.ec2.SecurityGroupEgressArgs(
        from_port=0,
        to_port=0,
        protocol="-1",
        cidr_blocks=["0.0.0.0/0"],
    )],
    tags={
        "Name": "VPC-SG",
    } 
)

instance = ec2.Instance("Instance1",
        ami=ami.id,
        instance_type="t2.micro",
        key_name=deployer,
        vpc_security_group_ids=[sg.id],
        subnet_id=public_subnet.id,

    credit_specification=aws.ec2.InstanceCreditSpecificationArgs(
        cpu_credits="unlimited",
    ),
    tags={
        "Name": "VPC-VM1",
    }
    )
instance2 = ec2.Instance("Instance2",
        ami=ami.id,
        instance_type="t2.micro",
        vpc_security_group_ids=[sg.id],
        key_name=deployer,
        subnet_id=private_subnet.id,

    credit_specification=aws.ec2.InstanceCreditSpecificationArgs(
        cpu_credits="unlimited",
    ),
    tags={
        "Name": "VPC-VM2",
    }
    )



pulumi.export("pulumi-az-amount", zones_amount)
pulumi.export("pulumi-vpc-id", vpc.id)
pulumi.export("pulumi-public-subnet-ids", public_subnet_ids)
pulumi.export("pulumi-private-subnet-ids", private_subnet_ids)
pulumi.export("pulumi-private-subnet-ids", private_subnet_ids)

pulumi.export("Instance1-IP", instance.private_ip)
pulumi.export("Instance1-Public-IP", instance.public_ip)
pulumi.export("Instance2-IP", instance2.private_ip)


